# Cermati Test

---

I'm using **React JS** (using typescript) as a framework to develop this app. Implementing isomorphic app, better debugging, customize everything from scratch.

## Project Structure

- ``src/client/components`` contains all shared components
- ``src/client/screens`` contains all the screens inside this app, structured by route logic
- ``src/server`` contains the client server

## Libraries
- Styling using `styled-components`
- Animation using `framer-motion`

## Server Side
- Implementing isomorphic app
- ``src/server/index.ts`` contains server config, both for development (run-time webpack compile) and production
- ``src/server/renderer.tsx`` contains rendering task used by both dev and prod env.

## TO RUN LOCALLY
- clone repo
- run `yarn install`
- run `yarn dev`
- open `http://localhost:4005`

## TO BUILD AND RUN PRODUCTION LOCALLY
- run `yarn build`
- run `yarn start`
- open `http://localhost:4006`