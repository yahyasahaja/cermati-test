import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { HighlightProp } from '../config';
import FAIcon from './FAIcon';

const Container = styled.div`
  display: block;
  padding: 15px;
  max-width: 400px;
  border: 1px solid #a0a0a0;
  margin: 10px;
  width: 100%;

  @media only screen and (min-width: 480px) {
    width: 45%;
  }

  @media only screen and (min-width: 960px) {
    width: 30%;
  }

  .card-top {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 10px;

    .card-title {
      font-size: 14pt;
      font-weight: 600;
    }
  }
`;

const NotificationPanel = (props: HighlightProp) => {
  const { description, iconName, iconPrefix, title } = props;

  return (
    <Container>
      <div className="card-top">
        <div className="card-title">{title}</div>
        <FAIcon name={iconName} prefix={iconPrefix} />
      </div>

      <div className="card-description">{description}</div>
    </Container>
  );
};

export default hot(module)(NotificationPanel);
