import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../config';

const Container = styled.button`
  display: block;
  margin: auto;
  padding: 10px 20px;
  background: ${COLORS.ORANGE};
  color: white;
  border: none;
  font-size: 11pt;
  cursor: pointer;

  &:active {
    background: ${COLORS.DARK_ORANGE};
  }
`;

const OrangeButton: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = (
  props
) => {
  const { children, ...others } = props;

  return <Container {...others}>{children}</Container>;
};

// OrangeButton.propTypes = a;

export default hot(module)(OrangeButton);
