import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../config';

const Container = styled.button`
  display: block;
  padding: 10px 20px;
  background: ${COLORS.BLUE};
  border-radius: 5px;
  color: white;
  cursor: pointer;
  font-size: 11pt;
  margin: 10px 0;

  &:active {
    background: ${COLORS.DARK_BLUE};
  }
`;

const NotificationPanel: React.FunctionComponent<React.ButtonHTMLAttributes<
  HTMLButtonElement
>> = (props) => {
  const { children, ...others } = props;

  return <Container {...others}>{children}</Container>;
};

export default hot(module)(NotificationPanel);
