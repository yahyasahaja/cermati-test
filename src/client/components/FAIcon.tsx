import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconPrefix, IconName } from '@fortawesome/fontawesome-svg-core';

const Icon = styled.div`
  font-size: 1.5rem;
  display: block;
  transition: 0.3s;
  color: #949494;
`;

type Props = {
  name: IconName;
  prefix: IconPrefix;
};

const FAIcon = (props: Props) => {
  const { name, prefix } = props;

  return (
    <Icon>
      <FontAwesomeIcon icon={[prefix, name]} />
    </Icon>
  );
};

export default React.memo(FAIcon);
