import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

import BlueButton from './BlueButton';

const Container = styled.div`
  max-width: 720px;
  margin: auto;
  overflow: hidden;

  .wrapper {
    padding: 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;

    .text {
      max-width: calc(720px - 120px);

      a {
        &:hover {
          text-decoration: underline;
        }
      }
    }
  }
`;

const MotionContainer = motion.custom(Container);

const NotificationPanel = () => {
  const [shouldShow, setShouldShow] = React.useState(true);

  return (
    <MotionContainer
      initial="visible"
      animate={shouldShow ? 'visible' : 'hidden'}
      variants={{
        hidden: {
          height: '0px',
        },
        visible: {
          height: 'auto',
        },
      }}
    >
      <div className="wrapper">
        <div className="text">
          By accessing and using this website, you acknowledge that you have
          read and understand our <a href="#">Cookie Policy</a>,{' '}
          <a href="#">Privacy Policy</a>, and our{' '}
          <a href="#">Terms of Service.</a>
        </div>

        <BlueButton
          onClick={() => {
            setShouldShow(false);
          }}
        >
          Got it
        </BlueButton>
      </div>
    </MotionContainer>
  );
};

export default hot(module)(NotificationPanel);
