import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../config';

const Container = styled.button`
  display: block;
  margin: auto;
  margin-top: 20px;
  padding: 10px 20px;
  background: none;
  color: white;
  border: 1px solid white;
  font-size: 11pt;
  cursor: pointer;
  transition: 0.3s;

  &:active {
    opacity: 0.5;
    transition: 0.01s;
  }

  &:hover {
    background: white;
    color: ${COLORS.DARK_BLUE};
  }
`;

const WhiteButton: React.FunctionComponent = (props) => {
  const { children, ...others } = props;

  return <Container {...others}>{children}</Container>;
};

export default hot(module)(WhiteButton);
