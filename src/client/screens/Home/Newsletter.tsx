import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

import OrangeButton from '../../components/OrangeButton';
import { hasWindow, calculateTimeDifferenceInMinutes } from '../../utils';
import { NEWSLETTER_PANEL_STATE_URI } from '../../config';

const Container = styled.div`
  display: block;
  position: fixed;
  bottom: 0;
  left: 0;
  max-width: 640px;
  color: white;
  background: rgba(0, 123, 193, 0.9);
  padding: 25px;
  z-index: 3;

  .close-button {
    font-size: 15pt;
    position: absolute;
    right: 10px;
    top: 10px;
    width: 20px;
    height: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;

    &:active {
      opacity: 0.5;
    }
  }

  .title {
    font-size: 15pt;
    margin: 10px 0;
  }

  .message {
    margin-top: 10px;
    margin-bottom: 20px;
  }

  .form {
    display: flex;
    justify-content: center;

    .email {
      flex: 1;
      font-size: 11pt;
      padding-left: 10px;
    }

    .countme-button {
      margin-left: 10px;
    }

    @media only screen and (max-width: 480px) {
      display: block;

      .email,
      .countme-button {
        width: 100%;
      }

      .email {
        height: 37px;
      }

      .countme-button {
        margin: 0;
        margin-top: 10px;
      }
    }
  }
`;

const MotionContainer = motion.custom(Container);

const Newsletter = () => {
  const [canShow, setCanShow] = React.useState(false);
  const [shouldShow, setShouldShow] = React.useState(false);

  React.useEffect(() => {
    if (hasWindow()) {
      const localLastClosed = Number(
        localStorage.getItem(NEWSLETTER_PANEL_STATE_URI)
      );

      if (!Number.isNaN(Number(localLastClosed)) && localLastClosed > 0) {
        const minutesDiff = calculateTimeDifferenceInMinutes(
          localLastClosed,
          Date.now()
        );

        if (minutesDiff > 10) {
          localStorage.removeItem(NEWSLETTER_PANEL_STATE_URI);
          setCanShow(true);
        }
      } else {
        setCanShow(true);
      }
    }
    // eslint-disable-next-line
  }, [canShow]);

  React.useEffect(() => {
    const handleScroll = () => {
      const currentPosition = window.scrollY;
      const height =
        window.document.documentElement.scrollHeight - window.innerHeight;

      if (canShow) {
        if (!shouldShow && currentPosition / height > 0.75) {
          setShouldShow(true);
        }
      }
    };

    if (hasWindow()) {
      document.addEventListener('scroll', handleScroll);
      return () => {
        document.removeEventListener('scroll', handleScroll);
      };
    }
  }, [shouldShow, canShow]);

  const handleClick = () => {
    setShouldShow(false);
    setCanShow(false);
    localStorage.setItem(NEWSLETTER_PANEL_STATE_URI, Date.now().toString());
  };

  return (
    <MotionContainer
      initial="hidden"
      transition={{
        duration: 0.3,
      }}
      animate={shouldShow ? 'visible' : 'hidden'}
      variants={{
        hidden: {
          transform: 'translateY(100%)',
        },
        visible: {
          transform: 'translateY(0%)',
        },
      }}
    >
      <div onClick={handleClick} className="close-button">
        &times;
      </div>
      <div className="title">Get latest updates in web technologies</div>
      <div className="message">
        I write articles related to web technologies, such as design trends,
        development tools, UI/UX case studies and reviews, and more. Sign up to
        my newsletter to get them all.
      </div>

      <form
        className="form"
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <input
          className="email"
          name="email"
          type="email"
          placeholder="Email address"
          required
        />
        <OrangeButton className="countme-button" type="submit">
          Count me in!
        </OrangeButton>
      </form>
    </MotionContainer>
  );
};

export default hot(module)(Newsletter);
