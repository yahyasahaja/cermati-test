import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { HIGHLIGHT_DATA } from '../../config';
import Card from '../../components/Card';

const Container = styled.div`
  display: block;
  padding: 20px;

  .header {
    padding: 20px;
    text-align: center;
    max-width: 600px;
    margin: auto;

    .header-title {
      margin-bottom: 20px;
      font-size: 15pt;
      font-weight: 500;
    }
  }

  .card-wrapper {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    max-width: 1366px;
    margin: auto;
  }
`;

const Header = () => {
  return (
    <Container>
      <div className="header">
        <div className="header-title">How Can I Help You?</div>

        <div className="header-description">
          Our work then targeted, best practices outcomes social innovation
          synergy. Venture philanthropy, revolutionary inclusive policymaker
          relief. User-centered program areas scale.
        </div>
      </div>

      <div className="card-wrapper">
        {HIGHLIGHT_DATA.map((highlight, i) => {
          return <Card {...highlight} key={i} />;
        })}
      </div>
    </Container>
  );
};

export default hot(module)(Header);
