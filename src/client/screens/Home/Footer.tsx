import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../../config';

const Container = styled.div`
  display: block;
  padding: 30px;
  background: #040325;
  color: #7f83a8;
  text-align: center;
`;

const Footer = () => {
  return <Container>&copy; 2020 Yahya Sahaja. All rights reserved</Container>;
};

export default hot(module)(Footer);
