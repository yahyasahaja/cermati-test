import { hot, setConfig } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';

import '../../icons';
import { COLORS } from '../../config';
import NotificationPanel from '../../components/NotificationPanel';
import Header from './Header';
import HighlightsPanel from './HighlightsPanel';
import Footer from './Footer';
import Newsletter from './Newsletter';
import { Helmet } from 'react-helmet';

setConfig({
  showReactDomPatchNotification: false,
});

const Container = styled.div`
  display: block;
  background-color: ${COLORS.SMOKE_GREY};
  color: ${COLORS.TEXT_COLOR};
  font-family: 'Open Sans', sans-serif;
  font-size: 11pt;
  min-height: 100vh;
`;

const App = () => {
  return (
    <Container>
      <Helmet>
        <title>Yahya Sahaja | Cermati.com Front-end Developer Entry Test</title>
      </Helmet>
      <NotificationPanel />
      <Header />
      <HighlightsPanel />
      <Footer />
      <Newsletter />
    </Container>
  );
};

export default hot(module)(App);
