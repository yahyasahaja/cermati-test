import { hot } from 'react-hot-loader';
import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../../config';
import WhiteButton from '../../components/WhiteButton';

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: url('/images/work-desk__dustin-lee.jpg');
  background-size: cover;
  position: relative;
  height: calc(100vh - 79px);

  .cover {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: ${COLORS.DARK_BLUE};
    opacity: 0.8;
    z-index: 0;
  }

  .logo {
    position: absolute;
    left: 10px;
    top: 10px;
    max-width: 50px;

    img {
      width: 100%;
    }
  }

  .hero-shot {
    display: block;
    text-align: center;
    z-index: 1;
    color: white;
    padding: 10px;

    .greet {
      display: block;
      font-size: 24pt;
      font-weight: 300;
    }

    .about {
      display: block;
      font-size: 20pt;
      font-weight: 600;
    }

    .message {
      display: block;
      margin-top: 10px;
    }
  }
`;

const Header = () => {
  return (
    <Container>
      <div className="cover" />
      <div className="logo">
        <img src="/images/y-logo-white.png" alt="" />
      </div>
      <div className="hero-shot">
        <div className="greet">Hello! I&apos;m Yahya Sahaja</div>
        <div className="about">Consult, Design, and Develop Websites</div>
        <div className="message">
          Have something great in mind? Feel free to contact me. <br />
          I&apos;ll help you to make it happen.
        </div>

        <WhiteButton>Let&apos;s Make Contact</WhiteButton>
      </div>
    </Container>
  );
};

export default hot(module)(Header);
