import { IconName, IconPrefix } from '@fortawesome/fontawesome-svg-core';

export const COLORS = {
  BLUE: '#007BC1',
  DARK_BLUE: '#004A75',
  ORANGE: '#FF8000',
  DARK_ORANGE: '#CC6600',
  SMOKE_GREY: '#E5E5E5',
  TEXT_COLOR: '#383839',
};
export const NEWSLETTER_PANEL_STATE_URI: string = 'newslettercermati';

export interface HighlightProp {
  title: string;
  iconName: IconName;
  iconPrefix: IconPrefix;
  description: string;
}

export const HIGHLIGHT_DATA: HighlightProp[] = [
  {
    title: 'Consult',
    iconName: 'comments',
    iconPrefix: 'fas',
    description:
      'Co-create, design thinking; strengthen infrastructure resist granular.' +
      'Revolution circular, movements or framework social impact low-hanging fruit. ' +
      'Save the world compelling revolutionary progress.',
  },
  {
    title: 'Design',
    iconName: 'paint-brush',
    iconPrefix: 'fas',
    description:
      'Policymaker collaborates collective impact humanitarian shared value' +
      'vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile ' +
      'issue outcomes vibrant boots on the ground sustainable.',
  },
  {
    title: 'Develop',
    iconName: 'cubes',
    iconPrefix: 'fas',
    description:
      'Revolutionary circular, movements a or impact framework social impact low-' +
      'hanging. Save the compelling revolutionary inspire progress. Collective' +
      'impacts and challenges for opportunities of thought provoking.',
  },
  {
    title: 'Marketing',
    iconName: 'bullhorn',
    iconPrefix: 'fas',
    description:
      'Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile,' +
      'replicable, effective altruism youth. Mobilize commitment to overcome' +
      'injustice resilient, uplift social transparent effective.',
  },
  {
    title: 'Manage',
    iconName: 'tasks',
    iconPrefix: 'fas',
    description:
      'Change-makers innovation or shared unit of analysis. Overcome injustice' +
      'outcomes strategize vibrant boots on the ground sustainable. Optimism,' +
      'effective altruism invest optimism corporate social.',
  },
  {
    title: 'Evolve',
    iconName: 'chart-line',
    iconPrefix: 'fas',
    description:
      'Activate catalyze and impact contextualize humanitarian. Unit of analysis' +
      'overcome injustice storytelling altruism. Thought leadership mass ' +
      'incarceration. Outcomes big data, fairness, social game-changer.',
  },
];

export default {
  COLORS,
  NEWSLETTER_PANEL_STATE_URI,
};
