import { library } from '@fortawesome/fontawesome-svg-core';
import { faComments } from '@fortawesome/free-solid-svg-icons/faComments';
import { faPaintBrush } from '@fortawesome/free-solid-svg-icons/faPaintBrush';
import { faCubes } from '@fortawesome/free-solid-svg-icons/faCubes';
import { faBullhorn } from '@fortawesome/free-solid-svg-icons/faBullhorn';
import { faTasks } from '@fortawesome/free-solid-svg-icons/faTasks';
import { faChartLine } from '@fortawesome/free-solid-svg-icons/faChartLine';

library.add(
  faComments,
  faPaintBrush,
  faCubes,
  faBullhorn,
  faTasks,
  faChartLine
);
