export const hasWindow = () => typeof window !== 'undefined';

export const useWindow = (windowProperty): any => {
  if (typeof window !== 'undefined') return window[windowProperty];
};

export const calculateTimeDifferenceInMinutes = (
  start: number,
  end: number
) => {
  const diffMs = end - start;
  const diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
  return diffMins;
};
