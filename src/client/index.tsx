import React from 'react';
import ReactDOM from 'react-dom';

import Home from './screens/Home';

if (typeof window !== 'undefined') {
  ReactDOM.hydrate(<Home />, document.getElementById('root'));
}

export const renderOnServer = () => {
  return <Home />;
};
