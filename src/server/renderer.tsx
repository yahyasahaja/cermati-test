import { RequestHandler } from 'express';
import { renderToString } from 'react-dom/server';
import { renderOnServer } from '../client';
import { Compiler } from 'webpack';
import path from 'path';
import { config } from './configs';
import fs from 'fs';
import { ServerStyleSheet } from 'styled-components';
import { Helmet } from 'react-helmet';

export default function serverRenderer(options): RequestHandler {
  let template = '';

  if (config.isDev) {
    const clientCompiler: Compiler = options.clientCompiler;
    const filename = path.join(clientCompiler.outputPath, 'template.html');
    (clientCompiler.outputFileSystem as any).readFile(filename, function (
      err,
      result
    ) {
      template = result.toString();
    });
  } else {
    template = fs
      .readFileSync(path.resolve('./build/client/template.html'))
      .toString();
  }

  return (req, res) => {
    const ReactComponent = renderOnServer();
    const helmet = Helmet.renderStatic();
    const styledSheets = new ServerStyleSheet();
    const result = renderToString(
      styledSheets.collectStyles(ReactComponent)
    );
    const html = template
      .toString()
      .replace('{{content}}', result)
      .replace('<head>', `<head>${helmet.title.toString()}`)
      .replace('</head>', `${styledSheets.getStyleTags()}</head>`);
    res.send(html);
    res.end();
  };
}
